"use strict";

const btnSwitchTheme = document.getElementById('btn-theme');
const body = document.querySelector('body');

if(localStorage.getItem('lightMode') === "true") {
    body.classList.add('light');
}

btnSwitchTheme.addEventListener('click', () => {
    body.classList.toggle('light');
    localStorage.setItem('lightMode', `${body.classList.contains("light")}`);
})

